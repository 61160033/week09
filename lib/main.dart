import 'package:flutter/material.dart';

import 'dog.dart';
import 'dog_dao.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  var fido = Dog(id: 0, name: 'Fido', age: 2);
  var dido = Dog(id: 1, name: 'Dido', age: 3);
  await DogDao.insertDog(fido);
  await DogDao.insertDog(dido);

  print(await DogDao.dogs());

  fido = Dog(
    id: fido.id,
    name: fido.name,
    age: fido.age + 7,
  );

  await DogDao.updateDog(fido);
  print(await DogDao.dogs());

  await DogDao.deleteDog(0);
  print(await DogDao.dogs());
}
